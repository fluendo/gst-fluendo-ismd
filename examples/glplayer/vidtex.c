//-----------------------------------------------------------------------------
/*
    This file is provided under a dual BSD/LGPLv2.1 license.  When using
    or redistributing this file, you may do so under either license.

    LGPL LICENSE SUMMARY

    Copyright(c) 2008. Intel Corporation. All rights reserved.
    Copyright(c) 2009, 2010. Fluendo S.A. All rights reserved.

    This library is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2.1 of the
    License.

    This library is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
    USA. The full GNU Lesser General Public License is included in this
    distribution in the file called LICENSE.LGPL.

    Contact Information for Intel:
        Intel Corporation
        2200 Mission College Blvd.
        Santa Clara, CA  97052

    Contat Information for Fluendo:
        FLUENDO S.A.
        World Trade Center Ed Norte 4 pl.
        Moll de Barcelona
        08039 BARCELONA - SPAIN

    BSD LICENSE

    Copyright (c) 2008. Intel Corporation. All rights reserved.
    Copyright(c) 2009, 2010. Fluendo S.A. All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

      - Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      - Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in
        the documentation and/or other materials provided with the
        distribution.
      - Neither the name of Intel Corporation nor the names of its
        contributors may be used to endorse or promote products derived
        from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "vidtex.h"
#include "ismd_gst_buffer.h"

// this is a GL extension function
PFNGLEGLIMAGETARGETTEXTURE2DOESPROC glEGLImageTargetTexture2DOES = NULL;

// Keep a frame count so we can wait a few frames before releasing
// buffers back to SMD. If we release the buffers too soon, SMD
// will overwrite them before graphics finishes rendering, resulting
// in tearing.
int framecount = 0;

// forward declarations
void add_reference (int id, ismd_buffer_handle_t handle);
void release_reference (int id);
int collect_garbage ();

// variables for models
int colorize = FALSE;
GLfloat alpha_value = 0.9F;
int animate_graphics = TRUE;

// CACHING
// -------
// Vidtex2 uses a single cache, but manages it at two levels.
//
// The first level takes the base pointers from the SMD port
// and maps them using gma_pixmap_alloc. These gma pixmaps
// are cached and reused whenever SMD reuses buffer, and are
// identified by the SMD base pointer.
//
// The second level wraps the gma pixmap with an EGL image
// to create a texture object. eglCreateImageKHR will run out
// of memory if we allocate too many images. In testing this
// occured around 35 images when running in 1080p, but this
// could vary with image size and memory configuration.
//
// EGL image memory can be managed by releasing it after a few
// frames, if this is an issue. We need to hold on for a few frames
// to give graphics time to render, so the collect_garbage()
// function releases egl images after a timed wait.
//
// The release_buffers() function releases references to the
// SMD buffer objects after a few frames, again to give
// graphics time to finish with it.
//
// The cache is managed as an array. The map_buffer function
// takes a base pointer and returns an array index (id) which
// is used throughout the code to manipulate buffer data.

// define this to release EGL images between uses
// if this is NOT defined, EGL images will be held and reused
#define RELEASE_IMAGES

// This is the number of entries in the cache array. This demo
// simply fails if it runs out of cache space.
#define MAP_CACHE_MAX 100

// Graphics plane size and position
// This is set by setup_gdl.
int gfx_plane_width;
int gfx_plane_height;
int gfx_forceplane = FALSE;

// Texture size. This is set only when we receive our first buffer
// from SMD and read the texture info it contains.
// The stride is hardcoded.
int src_texture_width;
int src_texture_height;
const int src_texture_stride = 2048;    // internal constant

//-------------------------------------------------------------------
// GStreamer
//-------------------------------------------------------------------

static void
on_pad_added (GstElement * element, GstPad * pad, gpointer data)
{
  GstPad *sinkpad;
  GstElement *pproc = (GstElement *) data;

  sinkpad = gst_element_get_static_pad (pproc, "sink");

  gst_pad_link (pad, sinkpad);

  gst_object_unref (sinkpad);
}

static void
on_eos (GstAppSink * sink, gpointer user_data)
{
  Player *player = (Player *) user_data;
  PLAYER_SET_EOS (player, TRUE);
}

static GstFlowReturn
on_buffer (GstAppSink * sink, gpointer user_data)
{
  Player *player = (Player *) user_data;
  PLAYER_SET_BUFFER (player, TRUE);
  return GST_FLOW_OK;
}

Player *
player_new (char *uri, int width, int height)
{
  GstElement *decoder, *pproc, *sink;
  GstAppSinkCallbacks appsink_cbs = { on_eos, on_buffer, on_buffer, NULL };
  Player *player = g_new0 (Player, 1);
  GstCaps *ismd_caps;

  /* Create gstreamer elements */
  player->pipeline = gst_pipeline_new ("video-player");
  decoder = gst_element_factory_make ("uridecodebin", "uridecodebin");
  pproc = gst_element_factory_make ("ismd_vidpproc", "vidpproc");
  sink = gst_element_factory_make ("appsink", "video-output");

  if (!player->pipeline || !decoder || !pproc || !sink) {
    g_printerr ("One element could not be created. Exiting.\n");
    g_free (player);
    return NULL;
  }

  /* Set up the pipeline */

  /* configure elements */
  ismd_caps = gst_caps_new_simple ("video/x-decoded-ismd", NULL);
  g_object_set (G_OBJECT (decoder), "uri", uri, "caps", ismd_caps, NULL);
  gst_caps_unref (ismd_caps);

  g_object_set (G_OBJECT (pproc), "rectangle", "0,0,540,384", NULL);

  gst_app_sink_set_callbacks (GST_APP_SINK (sink), &appsink_cbs, player, NULL);

  /* we add all elements into the pipeline */
  gst_bin_add_many (GST_BIN (player->pipeline), decoder, pproc, sink, NULL);

  /* we link the elements together */
  gst_element_link_many (pproc, sink, NULL);
  g_signal_connect (decoder, "pad-added", G_CALLBACK (on_pad_added), pproc);

  player->sink = gst_object_ref (sink);

  player_pause (player);

  return player;
}

void
player_free (Player * player)
{
  player_stop (player);
  gst_object_unref (GST_OBJECT (player->pipeline));
  gst_object_unref (player->sink);
  g_free (player);
}

void
player_play (Player * player)
{
  gst_element_set_state (player->pipeline, GST_STATE_PLAYING);
}

void
player_pause (Player * player)
{
  gst_element_set_state (player->pipeline, GST_STATE_PAUSED);
}

void
player_stop (Player * player)
{
  gst_element_set_state (player->pipeline, GST_STATE_NULL);
}

//-------------------------------------------------------------------
// OPEN GL
//-------------------------------------------------------------------

static void
setup_gl (void)
{
  //---------------------------------------------------------------
  // Set the GL viewport after scaling.
  //---------------------------------------------------------------
  float width = gfx_plane_width * gfxscale;
  float height = gfx_plane_height * gfxscale;
  float x = (gfx_plane_width - width) / 2.0f;
  float y = (gfx_plane_height - height) / 2.0f;

  glViewport (x, y, width, height);
  GLERR (glViewport);
  glClearColor (0.0f, 0.0f, 0.0f, 0.0f);
  GLERR (glClearColor);

  // clear once at program start
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  GLERR (glClear);
}

void
cleanup_gl (void)
{
  //---------------------------------------------------------------
  // Nothing to do here
  //---------------------------------------------------------------
}

//-------------------------------------------------------------------
// Structures needed for caching
//
// texture_st defines a texture; two of these are needed to render
// the Y plane (tex1) and the UV plane (tex2).
//
// cache_st defines a buffer containing two textures.
//-------------------------------------------------------------------

typedef struct texture_s
{
  // general stuff
  gma_pixel_format_t gma_format;
  GLuint gl_format;
  int texture_width;
  int texture_height;
  int texture_stride;           // multiple of 32 pix (128 bytes)
  int texture_bytes;            // bytes per pixel
  int buffer_offset;            // offset from base addr
  gma_pixmap_t pixmap;

  // egl stuff
  EGLImageKHR image;

  // gl stuff
  GLuint texture_id;            // GL texture id
} texture_st;

typedef struct cache_s
{
  ismd_buffer_handle_t handle;  // SMD buffer handle
  int phys_base;                // SMD base physical address
  void *virt_base;              // SMD base virtual address
  int size;                     // SMD buffer size

  texture_st tex1;              // texture info for Y plane
  texture_st tex2;              // texture info for UV plane

  int reffed;                   // SMD buffer handle is referenced
  int mapped;                   // buffer is mapped (gma_pixmap_alloc)
  int wrapped;                  // buffer is wrapped (eglCreateImageKHR)
  int framestamp;               // when it was reffed and wrapped
  // see add_reference(), collect_garbage()
} cache_st;

// some globals
int cache_count = 0;            // how many items are in the cache
cache_st array[MAP_CACHE_MAX];  // the cache array
cache_st *cache[MAP_CACHE_MAX]; // pointers into the cache array

int wrapped_count = 0;          // how many EGL images do we have?

//-------------------------------------------------------------------
// CACHING FUNCTIONS
//-------------------------------------------------------------------

void
init_cache (void)
{
  //---------------------------------------------------------------
  // Initialize the cache array to "empty" values.
  //---------------------------------------------------------------
  int i;
  cache_count = 0;
  memset (array, 0, sizeof (array));
  for (i = 0; i < MAP_CACHE_MAX; i++) {
    cache[i] = &array[i];
  }
}

static gma_ret_t
destroy_smd_pixmap (gma_pixmap_info_t * pixmap_info)
{
  // This sample application does not do anything in the GMA pixmap
  // destroy callback that it uses when wrapping SMD memory. It can
  // do this because it carefully manages the SMD buffers in the
  // following ways:
  //
  // 1) During playback, buffers are released back to SMD after the
  //    GPU is guaranteed to have read them. This is done by the
  //    free_cache() function, which compares the current frame number
  //    with the number of the last frame that used the texture. For
  //    a triple-buffered render target, a texture used in frame N
  //    is guaranteed to have been read by frame N+3.
  // 2) At shutdown, the application ensures that all rendering has
  //    completed before releasing EGL images, GMA pixmaps, and the
  //    SMD buffers. This occurs in graphics_fini(), which ensures
  //    that the GL context is no longer current and the render surface
  //    has been destroyed before proceeding with cleanup
  return GMA_SUCCESS;
}

gma_ret_t
create_gma_pixmap (texture_st * tex, unsigned long phys_addr, void *virt_addr)
{
  //---------------------------------------------------------------
  // Calls gma_pixmap_alloc. This has to be done for both
  // textures. Takes phys_addr as input and writes the output into
  // tex. This is a helper function for map_buffer()
  //
  // Returns a gma_ret_t error code.
  //---------------------------------------------------------------

  static gma_pixmap_funcs_t smd_pixmap_funcs = {
    .destroy = destroy_smd_pixmap
  };

  // maps a gma pixmap that wraps a user-provided buffer (phys_addr)
  gma_ret_t rc = GMA_SUCCESS;
  gma_pixmap_info_t pixmap_info = { 0 };

  pixmap_info.type = GMA_PIXMAP_TYPE_PHYSICAL;
  pixmap_info.virt_addr = virt_addr + tex->buffer_offset;
  pixmap_info.phys_addr = phys_addr + tex->buffer_offset;
  pixmap_info.width = tex->texture_width;
  pixmap_info.height = tex->texture_height;
  pixmap_info.pitch = tex->texture_stride * tex->texture_bytes;
  pixmap_info.format = tex->gma_format;
  pixmap_info.user_data = NULL;

  rc = gma_pixmap_alloc (&pixmap_info, &smd_pixmap_funcs, &tex->pixmap);
  if (rc != GMA_SUCCESS) {
    printf ("ERROR: gma_pixmap_alloc [0x%08lx] failed: %s (%d)\n",
        phys_addr, gma_error_string (rc), rc);
  }
  return rc;
}

int
map_buffer (unsigned long base, unsigned long size)
{
  //---------------------------------------------------------------
  // Input:  base address and size of SMD buffer
  // Returns: cache index
  //
  // Allocates a new cache entry, or returns existing entry.
  //---------------------------------------------------------------
  int id = -1;
  int i;

  // search for cached map using base address
  for (i = 0; i < cache_count; i++) {
    if (base == cache[i]->phys_base) {
      // found it, return id = index
      id = i;

      // if this buffer has been unmapped, re-map it
      if (cache[id]->mapped == FALSE) {
        gma_ret_t rc;

        cache[id]->virt_base =
            OS_MAP_IO_TO_MEM_CACHE (cache[id]->phys_base, cache[id]->size);
        while (NULL == cache[id]->virt_base && collect_garbage ()) {
          cache[id]->virt_base =
              OS_MAP_IO_TO_MEM_CACHE (cache[id]->phys_base, cache[id]->size);
        }

        rc = create_gma_pixmap (&cache[id]->tex1, base, cache[id]->virt_base);
        if (rc != GMA_SUCCESS)
          printf ("(error attempting to create texture 1)\n");
        rc = create_gma_pixmap (&cache[id]->tex2, base, cache[id]->virt_base);
        if (rc != GMA_SUCCESS)
          printf ("(error attempting to create texture 2)\n");

        cache[id]->mapped = TRUE;
      }
      // sanity check
      if (cache[id]->size != size)
        printf ("WARNING: size mismatch, %lu != %d\n", size, cache[id]->size);
      break;
    }
  }

  // if not cached...
  if (id == -1) {
    // if there is room in the cache, store it
    if (cache_count < MAP_CACHE_MAX) {
      // room in cache, map it and cache it
      i = cache_count++;

      // TODO: set texture_width correctly so that the shader
      // works with 0.0-1.0

      cache[i]->phys_base = base;
      cache[i]->size = size;

      // setup the texture memory params
      // y-plane, 8-bit
      cache[i]->tex1.gma_format = GMA_PF_A8;
      cache[i]->tex1.gl_format = GL_ALPHA;
      cache[i]->tex1.texture_width = 2048;
      cache[i]->tex1.texture_height = src_texture_height;
      cache[i]->tex1.texture_stride = 2048;
      cache[i]->tex1.texture_bytes = 1;
      cache[i]->tex1.buffer_offset = 0;

      // uv-plane, 16-bit
      cache[i]->tex2.gma_format = GMA_PF_AY16;
      cache[i]->tex2.gl_format = GL_LUMINANCE_ALPHA;
      cache[i]->tex2.texture_width = 1024;
      cache[i]->tex2.texture_height = src_texture_height;
      cache[i]->tex2.texture_stride = 1024;
      cache[i]->tex2.texture_bytes = 2;
      cache[i]->tex2.buffer_offset = src_texture_height * 2048;

      // create GMA pixmaps
      gma_ret_t rc;
      cache[i]->virt_base = OS_MAP_IO_TO_MEM_CACHE (cache[i]->phys_base,
          cache[i]->size);
      while (NULL == cache[i]->virt_base && collect_garbage ()) {
        cache[i]->virt_base = OS_MAP_IO_TO_MEM_CACHE (cache[i]->phys_base,
            cache[i]->size);
      }

      rc = create_gma_pixmap (&cache[i]->tex1, base, cache[i]->virt_base);
      if (rc != GMA_SUCCESS)
        printf ("(error attempting to create texture 1)\n");
      rc = create_gma_pixmap (&cache[i]->tex2, base, cache[i]->virt_base);
      if (rc != GMA_SUCCESS)
        printf ("(error attempting to create texture 2)\n");

      cache[i]->mapped = TRUE;
      id = i;
    } else {
      // no room in the cache
      printf ("ERROR: out of cache space!\n");
    }
  }
  if (i < MAP_CACHE_MAX) {
    cache[i]->reffed = TRUE;
  }

  return id;
}

void
unmap_buffer (int id)
{
  //---------------------------------------------------------------
  // Release gma pixmap associated with id and unmap buffer memory.
  //---------------------------------------------------------------
  if (cache[id]->wrapped) {
    printf ("ERROR: called unmap_buffer() on wrapped buffer\n");
  } else {
    if (cache[id]->mapped) {
      gma_ret_t rc;
      rc = gma_pixmap_release (&cache[id]->tex1.pixmap);
      GMAERR (rc, gma_pixmap_release);
      rc = gma_pixmap_release (&cache[id]->tex2.pixmap);
      GMAERR (rc, gma_pixmap_release);
      OS_UNMAP_IO_FROM_MEM (cache[id]->virt_base, cache[id]->size);
      cache[id]->mapped = FALSE;
    }
  }
}

void
init_texture (int id, texture_st * tex)
{
  //---------------------------------------------------------------
  // Creates an EGL image from a gma pixmap, gives it a GL
  // texture name, and sets the TEXTURE_2D parameters for it.
  // Helper function for wrap_buffer
  //---------------------------------------------------------------

  tex->image =
      eglCreateImageKHR (display, EGL_NO_CONTEXT, EGL_NATIVE_PIXMAP_KHR,
      (EGLClientBuffer) tex->pixmap, NULL);

  if (tex->image == EGL_NO_IMAGE_KHR) {
    EGLint err = eglGetError ();
    printf ("eglCreateImageKHR failed for id <%d/%d>: %s (%d)\n",
        id, cache_count, egl_error_string (err), err);
  }
  // can re-use textures so no need to do this twice
  if (tex->texture_id == 0) {
    glGenTextures (1, &tex->texture_id);
    GLERR (glGenTextures);
    glBindTexture (GL_TEXTURE_2D, tex->texture_id);
    GLERR (glBindTexture);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    GLERR (glTexParameteri);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    GLERR (glTexParameteri);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    GLERR (glTexParameteri);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    GLERR (glTexParameteri);
  }
}

void
wrap_buffer (int id)
{
  //---------------------------------------------------------------
  // Takes the cache id returned by map_buffer and allocates an
  // EGL image for it, if necessary.
  //---------------------------------------------------------------

  // skip if already initialized
  if (cache[id]->wrapped == FALSE) {
    init_texture (id, &cache[id]->tex1);
    init_texture (id, &cache[id]->tex2);
    cache[id]->wrapped = TRUE;
    wrapped_count++;
  }
}

void
unwrap_buffer (int id)
{
  //---------------------------------------------------------------
  // Reverse of wrap buffer. Destroys the image and the GL texture
  // names, and marks the cache entry as "unwrapped."
  //---------------------------------------------------------------

  if (cache[id]->wrapped == TRUE) {
    // destroy the GL texture handles
    glDeleteTextures (1, &cache[id]->tex1.texture_id);
    GLERR (glDeleteTextures);
    cache[id]->tex1.texture_id = 0;
    glDeleteTextures (1, &cache[id]->tex2.texture_id);
    GLERR (glDeleteTextures);
    cache[id]->tex2.texture_id = 0;

    // destroy the EGL images
    EGLBoolean ok;
    ok = eglDestroyImageKHR (display, cache[id]->tex1.image);
    if (!ok)
      EGLERR (eglDestroyImageKHR);
    ok = eglDestroyImageKHR (display, cache[id]->tex2.image);
    if (!ok)
      EGLERR (eglDestroyImageKHR);

    cache[id]->wrapped = FALSE;
    wrapped_count--;
  }
}

void
release_buffers ()
{
  //---------------------------------------------------------------
  // Releases SMD buffer locks after
  // a delay, which is a number of frames equal to 'age'.
  //---------------------------------------------------------------
  static const int ref_age = 3;

  // go through cache and release buffers 'age' frames old
  int i;
  for (i = 0; i < cache_count; i++) {
    if (cache[i]->framestamp <= framecount - ref_age) {
      if (cache[i]->reffed) {
        release_reference (i);
      }
    }
  }
}

int
collect_garbage ()
{
  //---------------------------------------------------------------
  // Frees egl image memory after
  // a delay, which is a number of frames equal to 'age'.
  // Returns 1 if a buffer was unmapped or 0 otherwise.
  //---------------------------------------------------------------
  static const int egl_age = 5;

  // go through cache and free buffers 'age' frames old
  int i;
  for (i = 0; i < cache_count; i++) {
#ifdef RELEASE_IMAGES
    if (cache[i]->framestamp <= framecount - egl_age) {
      if (cache[i]->wrapped) {
        unwrap_buffer (i);
      }
      if (cache[i]->mapped) {
        unmap_buffer (i);
        return TRUE;
      }
    }
#endif
  }
  return FALSE;
}

void
free_cache (void)
{
  //---------------------------------------------------------------
  // Frees everything in the cache.
  //---------------------------------------------------------------
  int i;
  for (i = 0; i < cache_count; i++) {
    if (cache[i]->wrapped) {
      unwrap_buffer (i);
    }
  }

  for (i = 0; i < cache_count; i++) {
    if (cache[i]->mapped)
      unmap_buffer (i);
  }

  for (i = 0; i < cache_count; i++) {
    if (cache[i]->reffed)
      release_reference (i);
  }

}

void
add_reference (int id, ismd_buffer_handle_t handle)
{
  //---------------------------------------------------------------
  // Add a reference to a single SMD buffer object
  //---------------------------------------------------------------
#if 0
  ismd_buffer_descriptor_t desc;
  ismd_buffer_read_desc (handle, &desc); 
  fprintf (stderr, "add_reference %08x in id=%d count=%d\n",
      handle, id, desc.reference_count);
#endif  
  ismd_buffer_add_reference (handle); 
  cache[id]->handle = handle;   // handle needed to release it
  cache[id]->reffed = TRUE;
  cache[id]->framestamp = framecount;
}

void
release_reference (int id)
{
  //---------------------------------------------------------------
  // release a reference to a single SMD buffer object
  //---------------------------------------------------------------
  assert (cache[id]->handle);

#if 0
  ismd_buffer_descriptor_t desc;
  ismd_buffer_read_desc (cache[id]->handle, &desc); 
  fprintf (stderr, "release_reference %08x in id=%d count=%d\n",
      cache[id]->handle, id, desc.reference_count);
#endif  
  ismd_buffer_dereference (cache[id]->handle);
  cache[id]->handle = 0;
  cache[id]->reffed = FALSE;
}

//-------------------------------------------------------------------
// UPDATE AND RENDER
//-------------------------------------------------------------------

void
graphics_render (int id)
{
  //---------------------------------------------------------------
  // render a frame
  //---------------------------------------------------------------

  if (cache[id]->wrapped != FALSE) {
    glActiveTexture (GL_TEXTURE0);
    GLERR (glActiveTexture);
    glBindTexture (GL_TEXTURE_2D, cache[id]->tex1.texture_id);
    GLERR (glBindTexture);
    glEGLImageTargetTexture2DOES (GL_TEXTURE_2D, cache[id]->tex1.image);
    GLERR (glEGLImageTargetTexture2DOES);

    glActiveTexture (GL_TEXTURE1);
    GLERR (glActiveTexture);
    glBindTexture (GL_TEXTURE_2D, cache[id]->tex2.texture_id);
    GLERR (glBindTexture);
    glEGLImageTargetTexture2DOES (GL_TEXTURE_2D, cache[id]->tex2.image);
    GLERR (glEGLImageTargetTexture2DOES);

    cube_render ();

    EGLBoolean ok;
    ok = eglSwapBuffers (display, surface);
    if (!ok)
      EGLERR (eglSwapBuffers);
  }
}

int
update_texture (ismd_buffer_handle_t handle, ismd_buffer_descriptor_t * desc)
{
  //---------------------------------------------------------------
  // Associates an SMD buffer with a cache entry, returns the id
  // Also performs gdl mapping and egl wrapping.
  //---------------------------------------------------------------

  //---------------------------------------------------------------
  // The pixel data is encoded in 4:2:2 YUV, so that the buffer
  // contains two planes: first the Y plane, followed by the UV plane.
  // Each plane contains a set of 8-bit values. The Y plane contains
  // one value per pixel, while the UV plane contains one U value and
  // one V value for every two pixels.
  // 
  // Example 4x4 pixel buffer (16 pixels total):
  //   A B C D
  //   E F G H
  //   I J K L
  //   M N O P
  //
  // In 32-bit RGBA encoding it would look like this:
  //
  //   Ar Ag Ab Aa Br Bg Bb Ba Cr Cg Cb Ca Dr Dg Db Da 
  //   Er Eg Eb Ea Fr Fg Fb Fa Gr Gg Gb Ga Hr Hg Hb Ha 
  //   Ir Ig Ib Ia Jr Jg Jb Ja Kr Kg Kb Ka Lr Lg Lb La 
  //   Mr Mg Mb Ma Nr Ng Nb Na Or Og Ob Oa Pr Pg Pb Pa
  //   (64 bytes total)
  //
  // In YUV encoding it would look like this:
  //
  //   Ay  By  Cy  Dy    <-- start of Y plane
  //   Ey  Fy  Gy  Hy
  //   Iy  Jy  Ky  Ly
  //   My  Ny  Oy  Py
  //   ABu ABv CDu CDv   <-- start of UV plane
  //   EFu EFv GHu GHv
  //   IJu IJv KLu KLv
  //   MNu MNv OPu OPv
  //
  //   Each plane contains 16 bytes with a stride of 4.
  //
  //   The U and V values are shared by two pixels. Thus:
  //   Pixel    Y-val U-val V-val
  //   A        Ay    ABu   ABv
  //   B        By    ABu   ABv
  //   C        Cy    CDu   CDv
  //   D        Dy    CDu   CDv
  //---------------------------------------------------------------

  // only do this once
  static int initialized = FALSE;
  if (!initialized) {
    initialized = TRUE;

    // need to typecast
    ismd_frame_attributes_t *attr =
        (ismd_frame_attributes_t *) desc->attributes;

    // set texture attributes
    src_texture_width = attr->dest_size.width;
    src_texture_height = attr->dest_size.height;

    // now that we know the texture dimensions we can setup the model
    cube_init ();
  }

  int id = map_buffer (desc->phys.base, // gma_pixmap_alloc
      desc->phys.size);         // & associate with cache id
  if (-1 != id) {
    wrap_buffer (id);           // eglCreateImageKHR
    add_reference (id, handle); // save buffer handle for later
  }
  return id;
}

int
graphics_update (Player * player)
{
  //---------------------------------------------------------------
  // update next graphics frame
  // 1. check to see if there is an SMD frame available
  // 2. if there is, render it to graphics
  // returns 'TRUE'  if it updated graphics,
  //         'FALSE' if no data or error
  //---------------------------------------------------------------
  GstBuffer *buffer = NULL;
  ismd_result_t rc = ISMD_SUCCESS;
  ismd_buffer_handle_t handle = 0;
  ismd_buffer_descriptor_t desc;

  int updated = FALSE;          // assume failure (this is our return value)
  int buffer_ready = FALSE;     // assume no data available

  // we either got a buffer, timed out, or have an error
  if (PLAYER_IS_BUFFER (player)) {
    if ((buffer = gst_app_sink_pull_buffer (GST_APP_SINK (player->sink)))) {
      handle = ((ISmdGstBuffer *) buffer)->ismd_buffer_id;
      // got a handle to a buffer, now get the descriptor
      rc = ismd_buffer_read_desc (handle, &desc);
      if (rc == ISMD_SUCCESS) {
        // got the buffer descriptor
        buffer_ready = TRUE;
      } else {
        // no buffer descriptor, abort
        printf ("ERROR: ismd_buffer_read_desc() returned %s (%d)\n",
            ismd_error_string (rc), rc);
        // dereference buffer since we'll not be using it after all
        rc = ismd_buffer_dereference (handle);
        if (rc != ISMD_SUCCESS) {
          printf ("graphics_update:ERROR: ismd_buffer_dereference() returned %s (%d)\n",
              ismd_error_string (rc), rc);
        }
      }
      gst_buffer_unref (buffer);
    }
    PLAYER_SET_BUFFER (player, FALSE);
  }
  // now we are finished with the SMD portion of the function
  // if the data is good, update the textures and render the frame
  if (buffer_ready) {
    // we only understand certain types of buffers
    if (desc.buffer_type == ISMD_BUFFER_TYPE_PHYS ||
        desc.buffer_type == ISMD_BUFFER_TYPE_VIDEO_FRAME ||
        desc.buffer_type == ISMD_BUFFER_TYPE_VIDEO_FRAME_REGION) {
      framecount++;             // use this to time release of handles
      int id = update_texture (handle, &desc);  // map buffer to texture
      if (-1 != id) {
        graphics_render (id);   // render the frame
        updated = TRUE;         // return success
      }
      release_buffers ();       // release old buffer handles
    } else {
      printf ("  desc.virt.base    = 0x%p\n", desc.virt.base);
    }
  }
  return (updated);
}

//-------------------------------------------------------------------
// GRAPHICS SETUP AND TEARDOWN
//-------------------------------------------------------------------

// globals used in this section
static int gfx_initted = FALSE;

void
graphics_init (gdl_plane_id_t gfxplane, int texwidth, int texheight)
{
  //---------------------------------------------------------------
  // initializes everything needed by graphics
  //---------------------------------------------------------------

  init_cache ();                // initialize buffer cache
  setup_gdl (gfxplane, texwidth, texheight);
  setup_egl ((NativeWindowType) gfxplane);
  setup_gl ();

  // this is part of GLES2, not EGL
  glEGLImageTargetTexture2DOES = (PFNGLEGLIMAGETARGETTEXTURE2DOESPROC)
      eglGetProcAddress ("glEGLImageTargetTexture2DOES");
  if (NULL == glEGLImageTargetTexture2DOES)
    printf ("eglGetProcAddress didn't find glEGLImageTargetTexture2DOES\n");

  gfx_initted = TRUE;
}

void
graphics_fini (void)
{
  //---------------------------------------------------------------
  // clean up everything associated with graphics
  //---------------------------------------------------------------
  if (gfx_initted) {
    cube_fini ();

    cleanup_gl ();
    stop_rendering ();

    free_cache ();

    cleanup_egl ();
    cleanup_gdl ();

    gfx_initted = FALSE;
  }
}
