/*
    This file is provided under a dual BSD/LGPLv2.1 license.  When using
    or redistributing this file, you may do so under either license.

    LGPL LICENSE SUMMARY

    Copyright(c) 2008. Intel Corporation. All rights reserved.
    Copyright(c) 2009, 2010. Fluendo S.A. All rights reserved.

    This library is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2.1 of the
    License.

    This library is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
    USA. The full GNU Lesser General Public License is included in this
    distribution in the file called LICENSE.LGPL.

    Contact Information for Intel:
        Intel Corporation
        2200 Mission College Blvd.
        Santa Clara, CA  97052

    Contat Information for Fluendo:
        FLUENDO S.A.
        World Trade Center Ed Norte 4 pl.
        Moll de Barcelona
        08039 BARCELONA - SPAIN

    BSD LICENSE

    Copyright (c) 2008. Intel Corporation. All rights reserved.
    Copyright(c) 2009, 2010. Fluendo S.A. All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

      - Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      - Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in
        the documentation and/or other materials provided with the
        distribution.
      - Neither the name of Intel Corporation nor the names of its
        contributors may be used to endorse or promote products derived
        from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/*-----------------------------------------------------------------------------
* matrix.c calculates all kinds of transformation matrices for OpenGL ES 2.0.
* For most functions, the first parameter m[16] is a column major output 
* matrix. The formulas used to calculate the transformation matrices and 
* projection matrices are based on the OpenGL ES specifications.
------------------------------------------------------------------------------*/
#ifndef MATRIX_H
#define MATRIX_H

/**
 * Normalize the vector to become a unit vector
 * Input: x, y, z --- input vector's x, y and z value
 * Output: fout[3]  --- output vector 
 */
float normalize (float fout[3], float x, float y, float z);

/**
 * Calculate the cross product of two input vectors
 * Input: src1[3], src2[3]  --- two input vectors
 * Output: fout[3]  --- output vector     
 */
void crossProduct (float fout[3], float src1[3], float src2[3]);

/**
 * Set the 4x4 matrix m[16] to the identity matrix
 */
void myIdentity (float m[16]);

/**
 * Multiply two 4x4 matrices
 * Input: src1[16], src2[16]  --- two source matrices
 * Output: m[16]              --- result matrix
 */
void myMultMatrix (float m[16], float src1[16], float src2[16]);

/**
 * Translate the 3D object in the scene
 * Input: tx , ty, tz    --- the offset for translation in X, Y, and Z dirctions 
 * Output: m[16]   --- the matrix after the translate
 */
void myTranslate (float m[16], float tx, float ty, float tz);

/**
 * Scale operation for the object in the scene 
 * Input: sx, sy, sz  --- scaling factors for X, Y and Z directions
 * Output: m[16]  --- the matrix after the scaling
 */
void myScale (float m[16], float sx, float sy, float sz);

/**
 * Rotation operation
 * Input: angle, x, y, z  (rotating angle and rotating axis)
 * Output: m[16]  --- the matrix after the rotation
 */
void myRotate (float m[16], float angle, float x, float y, float z);

/**
 * Perspective projection transformation
 * Input: left, right, bottom, top, near, far parameters similar to glFrustum 
 *        function
 * Output: m[16] ---  perspective projection matrix
 */
void myFrustum (float m[16], float left, float right, float bottom, float top,
    float near, float far);

/**
 * Orthogonal projection transformation
 * Input: left, right, bottom, top, near, far parameters similar to glOrtho 
 *        function
 * Output: m[16]  4x4 projection matrix
 */
void myOrtho (float m[16], float left, float right, float bottom, float top,
    float near, float far);

/**
 * Calculate the inverse tanspose matrix of a 4x4 matrix, and take the top 
 * left 3x3 matrix as output matrix.
 * Input: src[16]   4x4 matrix
 * Output: m[9]     3x3 matrix
 */
void myTransposeInvertMatrix (float m[16], float src[16]);

/**
 * Calculate the inverse matrix of a 4x4 matrix
 * Input: src[16]   4x4 matrix   
 * Output: m[16]    4x4 matrix
 */
void myInverseMatrix (float m[16], float src[16]);

/**
 * Similar to the gluLookAt function.
 * Input: eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ
 * Output: view matrix -- m[16]
 */
void myLookAt (float m[16], float eyeX, float eyeY, float eyeZ, float centerX,
    float centerY, float centerZ, float upX, float upY, float upZ);

#endif
