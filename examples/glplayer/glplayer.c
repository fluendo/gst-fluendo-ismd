/*
    This file is provided under a dual BSD/LGPLv2.1 license.  When using
    or redistributing this file, you may do so under either license.

    LGPL LICENSE SUMMARY

    Copyright(c) 2008. Intel Corporation. All rights reserved.
    Copyright(c) 2009, 2010. Fluendo S.A. All rights reserved.

    This library is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2.1 of the
    License.

    This library is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
    USA. The full GNU Lesser General Public License is included in this
    distribution in the file called LICENSE.LGPL.

    Contact Information for Intel:
        Intel Corporation
        2200 Mission College Blvd.
        Santa Clara, CA  97052

    Contat Information for Fluendo:
        FLUENDO S.A.
        World Trade Center Ed Norte 4 pl.
        Moll de Barcelona
        08039 BARCELONA - SPAIN

    BSD LICENSE

    Copyright (c) 2008. Intel Corporation. All rights reserved.
    Copyright(c) 2009, 2010. Fluendo S.A. All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

      - Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      - Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in
        the documentation and/or other materials provided with the
        distribution.
      - Neither the name of Intel Corporation nor the names of its
        contributors may be used to endorse or promote products derived
        from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

#include <sys/types.h>
#include <unistd.h>
#include <sched.h>

#include <signal.h>

#include "vidtex.h"

#define DEFAULT_WIDTH          1280
#define DEFAULT_HEIGHT          720
#define DEFAULT_PRESCALE          2
#define DEFAULT_GRAPHICS_PLANE GDL_PLANE_ID_UPP_C

//---------------------------------------------------------------------------
// initialization & cleanup
//---------------------------------------------------------------------------

static int is_initialized = FALSE;
static Player *player = NULL;

static void
main_initialize (void)
{
  // global initialization
  gdl_init (0);
  is_initialized = FALSE;
}

static void
main_cleanup (void)
{
  // only do this once at shutdown
  if (is_initialized) {
    is_initialized = FALSE;

    // per-video cleanup
    if (player) {
      player_free (player);
      player = NULL;
    }
    // must stop graphics first so it releases the output port
    graphics_fini ();

    printf ("gdl_close()\n");
    gdl_close ();
  }
}

//---------------------------------------------------------------------------
// signal and exit handling
//---------------------------------------------------------------------------

static int quitflag = FALSE;

static void
sighandler (int signum)
{
  // report what signal we caught
  sleep (1);                    // allow other threads to print
  switch (signum) {
#define ONCASE(x) case x: printf("\nCaught signal %s in %s:%d\n", #x, __FILE__,__LINE__); break
      ONCASE (SIGINT);          /* CTRL-C   */
      ONCASE (SIGQUIT);         /* CTRL-\   */
      ONCASE (SIGILL);
      ONCASE (SIGABRT);         /* abort()  */
      ONCASE (SIGFPE);
      ONCASE (SIGSEGV);
      ONCASE (SIGTERM);
#undef ONCASE
    default:
      printf ("\nCaught unknown signal %d in %s:%d\n", signum, __FILE__,
          __LINE__);
      break;
  }
  signal (signum, SIG_DFL);     // restore default handler

  // perform a graceful cleanup
  if (is_initialized)
    main_cleanup ();
  else
    printf ("\nSignal caught during main_cleanup()\n");

  // send the signal to the default signal handler, to allow a debugger
  // to trap it
  kill (getpid (), signum);     // invoke default handler
  quitflag = 1;
}

static void
setup_exit (void)
{
  // cannot catch SIGKILL or SIGSTOP
  signal (SIGINT, sighandler);  // CTRL-C
  signal (SIGQUIT, sighandler);
  signal (SIGILL, sighandler);
  signal (SIGABRT, sighandler);
  signal (SIGFPE, sighandler);
  signal (SIGSEGV, sighandler);
  signal (SIGTERM, sighandler);
  atexit (main_cleanup);
}

gboolean
get_plane_id (char *spec, gdl_plane_id_t * plane)
{
  if (spec[0] != '\0' && spec[1] == '\0') {
    switch (spec[0]) {
      case 'a':
      case 'A':
        *plane = GDL_PLANE_ID_UPP_A;
        break;
      case 'b':
      case 'B':
        *plane = GDL_PLANE_ID_UPP_B;
        break;
      case 'c':
      case 'C':
        *plane = GDL_PLANE_ID_UPP_C;
        break;
      case 'd':
      case 'D':
        *plane = GDL_PLANE_ID_UPP_D;
        break;
      default:
        return FALSE;
    }
    return FALSE;
  }
  return FALSE;
}

float gfxscale = 0.8F;

void
usage (char *progname)
{
  printf ("usage: %s [OPTIONS] URI\n", progname);
  printf ("options:\n");
  printf ("  -h       show usage information\n");
  printf ("  -G X     set graphics plane to A, B, C, or D (default %c)\n",
      plane_id (DEFAULT_GRAPHICS_PLANE));
  printf ("  URI      is an standard GStreamer URI\n");
}

int
main (int argc, char **argv)
{
  int opt;
  int width = DEFAULT_WIDTH;
  int height = DEFAULT_HEIGHT;
  int prescale = DEFAULT_PRESCALE;
  int width_ps = width;
  int height_ps = height;
  gdl_plane_id_t g_plane = DEFAULT_GRAPHICS_PLANE;
  char *uri = NULL;

  gst_init (&argc, &argv);

  // parse command-line options
  while ((opt = getopt (argc, argv, "h:G:")) != -1)
    switch (opt) {

        // -h display usage summary
      case 'h':
        usage (argv[0]);
        exit (0);

        // set graphics plane
      case 'G':
        if (!get_plane_id (optarg, &g_plane)) {
          printf ("Invalid specification for graphics plane\n");
          return 0;
        }
        break;

      default:
        exit (1);               // error
    }

  // grab non-option arguments from the command line
  if (optind < argc) {
    uri = argv[optind];
    optind++;
  }
  // sanity check and debug dump
  if (uri == NULL) {
    printf ("No URI provided\n");
    return 1;
  }
  // scale the video to designated size
  width_ps = width / prescale;
  height_ps = height / prescale;
  printf ("Set DPE prescaling to %dx%d... (%d)\n", width_ps, height_ps,
      prescale);

  // initialize
  setup_exit ();
  main_initialize ();

  // play the movie
  if ((player = player_new (uri, width_ps, height_ps))) {
    printf ("Initializing graphics\n");
    graphics_init (g_plane, width_ps, height_ps);

    player_play (player);

    while (!quitflag && !PLAYER_IS_EOS (player)) {
      if (graphics_update (player)) {
        cube_update ();
      } else {
        // idle
        os_sleep(10);
        //sched_yield ();
      }
    }
  } else {
    printf ("Failed to initialize\n");
  }

  return 0;
}
